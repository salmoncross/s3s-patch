# s3s-patch 🐙

# 使用本项目前请确保你能流畅使用google.com

本项目仅为s3s的本地数据存储补丁，原项目请查看[s3s](https://github.com/frozenpandaman/s3s)


### 使用说明

1、前往[python官网](https://www.python.org/downloads/)下载适合你系统的python

2、将 local-example.db 重命名为 local.db

3、安装依赖
```
pip install -r requirements.txt
```

4、下载s3s

```
git clone https://github.com/frozenpandaman/s3s.git
```

5、将s3s中post_result函数替换为s3s-example.py中post_result函数

6、在s3s.py头部导入

```
from patch import SaveBattleData, SaveSalmonRunData
```

7、将s3s中iksm.py,s3s.py,utils.py文件移动至s3s-patch/server.py同级

8、启动s3s（具体使用说明https://github.com/frozenpandaman/s3s）
```
python s3s.py -M 60
```
 * Running on http://127.0.0.1:5000
   * 本地访问
 * Running on http://192.168.0.1:5000   
   * 局域网访问

9、启动s3s-patch
```
python server.py
```

### 其他

点击链接加入群聊【斯普拉遁3直播插件】：http://qm.qq.com/cgi-bin/qm/qr?_wv=1027&k=LRmBDiJ_KNWzJC5jPln_6ecen80HFdCB&authKey=ROr6TTgLYaVeRXT2kYyO0%2FNnssoQH7SHyluT0eGAf7zhBdigSAHhkPZKq9CdJXPl&noverify=0&group_code=754442676

仅提供直播项目美化的交流建议，不提供技术支持

https://google.com/

https://www.bing.com/

https://www.baidu.com/
