import json
from flask import Flask, jsonify, render_template
import sqlite3

app = Flask(__name__)

@app.route('/')
def getIndexHtml():
    return render_template('index.html')

@app.route('/battle/simple')
def getBattleSimpleHtml():
    return render_template('battle-simple.html')

@app.route('/battle/simple/json')
def getBattleSimpleJson():
    conn = sqlite3.connect('local.db')
    cursor = conn.cursor()
    searchSql="select * from battle order by end_at desc limit 1"
    cursor.execute(searchSql)
    searchResult=cursor.fetchall()
    cursor.close()
    conn.close()
    data = {
            'kill': searchResult[0][8],
            'assist': searchResult[0][9],
            "death":searchResult[0][11],
            "special":searchResult[0][12],
        }
    return jsonify(data)

@app.route('/battle/all')
def getBattleAllHtml():
    return render_template('battle-all.html')

@app.route('/battle/all/json')
def getBattleAllJson():
    conn = sqlite3.connect('local.db')
    cursor = conn.cursor()
    searchSql="select sum(kill),sum(assist),sum(death),sum(special) from battle where strftime('%Y-%m-%d', datetime(start_at, 'unixepoch'))=date();"
    cursor.execute(searchSql)
    searchResult=cursor.fetchall()
    cursor.close()
    conn.close()
    data = {
            'kill': searchResult[0][0],
            'assist': searchResult[0][1],
            "death":searchResult[0][2],
            "special":searchResult[0][3],
        }
    return jsonify(data)

@app.route('/battle/avg')
def getBattleAvgHtml():
    return render_template('battle-avg.html')

@app.route('/battle/avg/json')
def getBattleAvgJson():
    conn = sqlite3.connect('local.db')
    cursor = conn.cursor()
    searchSql="select avg(kill),avg(assist),avg(death),avg(special) from battle where strftime('%Y-%m-%d', datetime(start_at, 'unixepoch'))=date();"
    cursor.execute(searchSql)
    searchResult=cursor.fetchall()
    cursor.close()
    conn.close()
    data = {
            'kill': searchResult[0][0],
            'assist': searchResult[0][1],
            "death":searchResult[0][2],
            "special":searchResult[0][3],
        }
    return jsonify(data)

@app.route('/x_power/simple')
def getXPowerSimpleHtml():
    return render_template('x-power-simple.html')

@app.route('/x_power/simple/json')
def getXPowerSimpleJson():
    conn = sqlite3.connect('local.db')
    cursor = conn.cursor()
    searchSql="select * from battle order by end_at desc limit 1"
    cursor.execute(searchSql)
    searchResult=cursor.fetchall()
    cursor.close()
    conn.close()
    data = {
            'x_power_before': searchResult[0][36],
            'x_power_after': searchResult[0][37],
        }
    return jsonify(data)

@app.route('/coop/enemy')
def getCoopEnemyHtml():
    return render_template('coop-enemy.html')

@app.route('/coop/enemy/json')
def getCoopEnemyJson():
    conn = sqlite3.connect('local.db')
    cursor = conn.cursor()
    searchSql="select * from salmon_run order by start_at desc limit 1"
    cursor.execute(searchSql)
    searchResult=cursor.fetchall()
    cursor.close()
    conn.close()
    return searchResult[0][26]

@app.route('/coop/bosses')
def getCoopAllBossHtml():
    return render_template('coop-bosses.html')

@app.route('/coop/bosses/json')
def getCoopBossesJson():
    conn = sqlite3.connect('local.db')
    cursor = conn.cursor()
    searchSql="SELECT sum(json_extract(bosses, '$.bakudan.defeated_by_me')) AS bakudan,sum(json_extract(bosses, '$.katapad.defeated_by_me')) AS katapad,sum(json_extract(bosses, '$.teppan.defeated_by_me')) AS teppan,sum(json_extract(bosses, '$.hebi.defeated_by_me')) AS hebi,sum(json_extract(bosses, '$.tower.defeated_by_me')) AS tower,sum(json_extract(bosses, '$.mogura.defeated_by_me')) AS mogura,sum(json_extract(bosses, '$.koumori.defeated_by_me')) AS koumori,sum(json_extract(bosses, '$.hashira.defeated_by_me')) AS hashira,sum(json_extract(bosses, '$.diver.defeated_by_me')) AS diver,sum(json_extract(bosses, '$.nabebuta.defeated_by_me')) AS nabebuta FROM salmon_run"
    cursor.execute(searchSql)
    searchResult=cursor.fetchall()
    cursor.close()
    conn.close()
    data = []
    for row in searchResult:
        item = {
            'bakudan': row[0],
            'katapad': row[1],
            'teppan': row[2],
            'hebi': row[3],
            'tower': row[4],
            'mogura': row[5],
            'koumori': row[6],
            'hashira': row[7],
            'diver': row[8],
            'nabebuta': row[9],
        }
    data.append(item)
    json_data = json.dumps(data)
    return json_data

@app.route('/heat_map')
def getHeatMap():
    return render_template('heat-map.html')

@app.route('/heat_map/json')
def getHeatMapJson():
    conn = sqlite3.connect('local.db')
    cursor = conn.cursor()
    searchSql="select strftime('%Y-%m-%d', datetime(start_at, 'unixepoch')) as date , COUNT(*) as total from battle GROUP BY strftime('%Y-%m-%d', datetime(start_at, 'unixepoch'))"    
    cursor.execute(searchSql)
    searchResult=cursor.fetchall()
    cursor.close()
    conn.close()
    result={}
    for item in searchResult:
        result[item[0]]=item[1]
    return (result)

@app.route('/win_lose')
def getWinLose():
    return render_template('win-lose.html')

@app.route('/win_lose/json')
def getWinLoseJson():
    conn = sqlite3.connect('local.db')
    cursor = conn.cursor()
    searchSql="SELECT SUM(CASE WHEN result = 'win' THEN 1 ELSE 0 END) AS win,SUM(CASE WHEN result = 'lose' THEN 1 ELSE 0 END) AS lose,COUNT(*) AS total FROM battle where strftime('%Y-%m-%d', datetime(start_at, 'unixepoch'))=date();"    
    cursor.execute(searchSql)
    searchResult=cursor.fetchall()
    cursor.close()
    conn.close()
    data = {
            'win': searchResult[0][0],
            'lose': searchResult[0][1],
            'total':searchResult[0][2],
        }
    return jsonify(data)

if __name__ == '__main__':
    app.run('0.0.0.0',5000)