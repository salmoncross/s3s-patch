import sqlite3
import json

conn = sqlite3.connect('local.db')

def SaveBattleData(payload: any):
    cursor = conn.cursor()
    searchSql="select * from battle where uuid='"+payload["uuid"]+"'"
    cursor.execute(searchSql)
    searchResult=cursor.fetchall()
    if len(searchResult)<=0:
        battleData = [
        	payload.get("uuid", 0),
        	payload.get("lobby", 0),
        	payload.get("rule", 0),
        	payload.get("stage", 0),
        	payload.get("weapon", 0),
        	payload.get("result", 0),
        	payload.get("knockout", 0),
        	payload.get("rank_in_team", 0),
        	payload.get("kill", 0),
        	payload.get("assist", 0),
        	payload.get("kill_or_assist", 0),
        	payload.get("death", 0),
        	payload.get("special", 0),
        	payload.get("signal", 0),
        	payload.get("inked", 0),
            json.dumps(payload.get("medals", 0)),
        	payload.get("our_team_inked", 0),
        	payload.get("their_team_inked", 0),
        	payload.get("third_team_inked", 0),
        	payload.get("our_team_percent", 0),
        	payload.get("their_team_percent", 0),
        	payload.get("third_team_percent", 0),
        	payload.get("our_team_count", 0),
        	payload.get("their_team_count", 0),
        	payload.get("level_before", 0),
        	payload.get("level_after", 0),
        	payload.get("rank_before", 0),
        	payload.get("rank_before_s_plus", 0),
        	payload.get("rank_before_exp", 0),
        	payload.get("rank_after", 0),
        	payload.get("rank_after_s_plus", 0),
        	payload.get("rank_after_exp", 0),
        	payload.get("rank_exp_change", 0),
        	payload.get("rank_up_battle", 0),
        	payload.get("challenge_win", 0),
        	payload.get("challenge_lose", 0),
        	payload.get("x_power_before", 0),
        	payload.get("x_power_after", 0),
        	payload.get("fest_power", 0),
        	payload.get("fest_dragon", 0),
        	payload.get("clout_before", 0),
        	payload.get("clout_after", 0),
        	payload.get("clout_change", 0),
        	payload.get("cash_before", 0),
        	payload.get("cash_after", 0),
        	payload.get("our_team_color", 0),
        	payload.get("their_team_color", 0),
        	payload.get("third_team_color", 0),
        	payload.get("our_team_role", 0),
        	payload.get("their_team_role", 0),
        	payload.get("third_team_role", 0),
        	payload.get("our_team_theme", 0),
        	payload.get("their_team_theme", 0),
        	payload.get("third_team_theme", 0),
        	json.dumps(payload.get("our_team_players", 0)),
        	json.dumps(payload.get("their_team_players", 0)),
        	json.dumps(payload.get("third_team_players", 0)),
        	payload.get("start_at", 0),
        	payload.get("end_at", 0),
    	]
        cursor.execute("insert into battle values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", battleData)
        conn.commit()# conn.close()
        cursor.close()

def SaveSalmonRunData(payload:any):
    cursor = conn.cursor()
    searchSql="select * from battle where uuid='"+payload["uuid"]+"'"
    cursor.execute(searchSql)
    searchResult=cursor.fetchall()
    if len(searchResult)<=0:
        salmonRunData=[
        	payload.get("uuid", 0),
        	payload.get("private", 0),
        	payload.get("big_run", 0),
        	payload.get("eggstra_work", 0),
        	payload.get("stage", 0),
        	payload.get("danger_rate", 0),
        	payload.get("clear_waves", 0),
        	payload.get("fail_reason", 0),
        	payload.get("king_smell", 0),
        	payload.get("king_salmonid", 0),
        	payload.get("clear_extra", 0),
        	payload.get("title_before", 0),
        	payload.get("title_exp_before", 0),
        	payload.get("title_after", 0),
        	payload.get("title_exp_after", 0),
        	payload.get("golden_eggs", 0),
        	payload.get("power_eggs", 0),
        	payload.get("gold_scale", 0),
        	payload.get("silver_scale", 0),
        	payload.get("bronze_scale", 0),
        	payload.get("job_point", 0),
        	payload.get("job_score", 0),
        	payload.get("job_rate", 0),
        	payload.get("job_bonus", 0),
        	json.dumps(payload.get("waves", 0)),
        	json.dumps(payload.get("players", 0)),
        	json.dumps(payload.get("bosses", 0)),
        	payload.get("start_at", 0),
        	payload.get("end_at", 0),
        ]
        cursor.execute("insert into salmon_run values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",salmonRunData)
        conn.commit()# conn.close()
        cursor.close()