# 导入对应函数
from patch import SaveBattleData, SaveSalmonRunData

# 替换 s3s.py 中对应函数
def post_result(data, ismonitoring, isblackout, istestrun, overview_data=None):
	'''Uploads battle/job JSON to stat.ink, and prints the returned URL or error message.'''
	if isinstance(data, list): # -o export format
		try:
			data = [x for x in data if x["data"]["vsHistoryDetail"] is not None] # avoid {"data": {"vsHistoryDetail": None}} error
			results = sorted(data, key=lambda d: d["data"]["vsHistoryDetail"]["playedTime"])
		except KeyError:
			try:
				data = [x for x in data if x["data"]["coopHistoryDetail"] is not None]
				results = sorted(data, key=lambda d: d["data"]["coopHistoryDetail"]["playedTime"])
			except KeyError: # unsorted - shouldn't happen
				print("(!) Uploading without chronologically sorting results")
				results = data
	elif isinstance(data, dict):
		try:
			results = data["results"]
		except KeyError:
			results = [data] # single battle/job - make into a list

	# filter down to one battle at a time
	for i in range(len(results)):
		if "vsHistoryDetail" in results[i]["data"]: # ink battle
			payload = prepare_battle_result(results[i]["data"], ismonitoring, isblackout, overview_data)
			which = "ink"
		elif "coopHistoryDetail" in results[i]["data"]: # salmon run job
			prevresult = results[i-1]["data"] if i > 0 else None
			payload = prepare_job_result(results[i]["data"], ismonitoring, isblackout, overview_data, prevresult=prevresult)
			which = "salmon"
		else: # shouldn't happen
			print("Ill-formatted JSON while uploading. Exiting.")
			print('\nDebug info:')
			print(json.dumps(results))
			sys.exit(1) # always exit here - something is seriously wrong

		if len(payload) == 0: # received blank payload from prepare_job_result() - skip unsupported battle
			continue

		# should have been taken care of in fetch_json() but just in case...
		if payload.get("lobby") == "private" and utils.custom_key_exists("ignore_private", CONFIG_DATA) or \
			payload.get("private") == "yes" and utils.custom_key_exists("ignore_private_jobs", CONFIG_DATA): # SR version
			continue

		s3s_values = {'agent': '\u0073\u0033\u0073', 'agent_version': f'v{A_VERSION}'} # lol
		s3s_values["agent_variables"] = {'Upload Mode': "Monitoring" if ismonitoring else "Manual"}
		payload.update(s3s_values)

		if payload["agent"][0:3] != os.path.basename(__file__)[:-3]:
			print("Could not upload. Please contact @frozenpandaman on GitHub for assistance.")
			sys.exit(0)

		if istestrun:
			payload["test"] = "yes"

		if which == "ink":
			SaveBattleData(payload)
		elif which == "salmon":
			SaveSalmonRunData(payload)